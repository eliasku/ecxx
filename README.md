# ECXX

[**ECXX**](https://gitlab.com/eliasku/ecxx) is Entity Component System library for C++17 inspired by [**EnTT**](https://github.com/skypjack/entt). Self-educational project to learn C++17 features and to understand core techniques applied at *EnTT* library.

---------

**Work in progress.**

- [ecxx wiki](https://gitlab.com/eliasku/ecxx/wikis/home)
    - [How to Install](https://gitlab.com/eliasku/ecxx/wikis/Install)
    - [Examples](https://gitlab.com/eliasku/ecxx/wikis/Examples)