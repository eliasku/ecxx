#include <ecxx/ecxx.hpp>
#include <iostream>

int main() {
    std::cout << "test package running...\n";
    ecs::world w;
    auto e = w.create();
    return w.valid(e) ? 0 : -1;
}
