from conans import ConanFile, CMake, tools


class TestPackageConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def _configure_cmake(self):
        defs = {}

        if tools.get_env("CMAKE_SKIP_COMPILER_CHECKS", True):
            defs['CMAKE_C_COMPILER_FORCED'] = 'TRUE'
            defs['CMAKE_CXX_COMPILER_FORCED'] = 'TRUE'

        cmake = CMake(self)
        cmake.configure(defs=defs)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def test(self):
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake = self._configure_cmake()
            cmake.test(output_on_failure=True)
