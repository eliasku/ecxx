#!/usr/bin/env bash

set -e
set -x

export BUILD_ID=coverage
export CONAN_BUILD_COVERAGE=1
export BUILD_DIR=builds/${BUILD_ID}

conan install -s build_type=Debug -pr=${CONAN_BUILD_PROFILE} -if ${BUILD_DIR}/debug -b outdated .
conan build -bf ${BUILD_DIR}/debug -c -b .

pushd ${BUILD_DIR}/debug
ctest -j 8 --output-on-failure
popd

lcov --rc lcov_branch_coverage=1 --capture --directory ${BUILD_DIR}/debug --output-file ${BUILD_DIR}/coverage.all.info
lcov --rc lcov_branch_coverage=1 --extract ${BUILD_DIR}/coverage.all.info "${LCOV_FILTER}" --output-file ${BUILD_DIR}/coverage.info
genhtml --branch-coverage -o ${BUILD_DIR}/report ${BUILD_DIR}/coverage.info --legend --title 'coverage'
