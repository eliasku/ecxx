#!/usr/bin/env bash

set -e
set -x

conan remote add -f skypjack https://api.bintray.com/conan/skypjack/conan

PROJ_DIR=benchmarks
BUILD_ID=benchmarks
BUILD_DIR=builds/${BUILD_ID}/release
CONAN_BUILD_PROFILE=${CONAN_BUILD_PROFILE:=default}
conan install -pr ${CONAN_BUILD_PROFILE} -if ${BUILD_DIR} -s build_type=Release -b missing ${PROJ_DIR}
conan build -bf ${BUILD_DIR} -c -b ${PROJ_DIR}

./${BUILD_DIR}/bin/benchmarks