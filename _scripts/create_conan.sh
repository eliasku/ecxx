#!/usr/bin/env bash

set -e
set -x

conan remove --force "${CONAN_PACKAGE_NAME}"'*'

conan install -s build_type=Debug -pr=${CONAN_BUILD_PROFILE} -if builds/_deps/debug -b=missing .
conan install -s build_type=Release -pr=${CONAN_BUILD_PROFILE} -if builds/_deps/release -b=missing .

conan create -s build_type=Debug -pr=${CONAN_BUILD_PROFILE} . ${CONAN_USER_CHANNEL}
conan create -s build_type=Release -pr=${CONAN_BUILD_PROFILE} . ${CONAN_USER_CHANNEL}

conan upload --all -c -r=$CONAN_REMOTE ${CONAN_PACKAGE_NAME}
