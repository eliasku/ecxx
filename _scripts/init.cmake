option(BUILD_TESTS "Build unit tests" ON)
option(BUILD_COVERAGE "Build coverage check" OFF)

message(INFO "[${PROJECT_NAME}] loading `${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake`")
include(${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)
message(INFO "[${PROJECT_NAME}] =>> CONAN_LIBS = ${CONAN_LIBS}")

if (BUILD_TESTS AND BUILD_COVERAGE AND CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -g -fno-inline -fprofile-arcs -ftest-coverage -fno-omit-frame-pointer -fno-optimize-sibling-calls")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
    add_definitions(-DNDEBUG) # ignore assertation coverage
endif ()