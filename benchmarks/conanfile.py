from conans import ConanFile, CMake, tools


class ecxx_benchmarks_conan(ConanFile):
    name = "ecxx_benchmarks"
    version = "0.0.1"
    license = "ISC"
    url = "https://gitlab.com/eliasku/ecxx"
    description = "ecxx / entt benchmarks project"
    topics = ("ecs", "entity", "component", "system", "benchmarks")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = ["CMakeLists.txt", "*.hpp", "*.cpp"]
    no_copy_source = True
    requires = [
        "entt/3.0.0@skypjack/stable"
    ]

    def _configure_cmake(self):
        defs = {}

        if tools.get_env("CMAKE_SKIP_COMPILER_CHECKS", True):
            defs['CMAKE_C_COMPILER_FORCED'] = 'TRUE'
            defs['CMAKE_CXX_COMPILER_FORCED'] = 'TRUE'

        cmake = CMake(self)
        cmake.configure(defs=defs)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()