from conans import python_requires

ek_conanfile = python_requires("ek-conanfile/1.1.4@eliasku/testing")

class ecxx_conan(ek_conanfile.EkConanFile):
    name = "ecxx"
    version = "1.1.0"
    license = "ISC"
    author = "eliasku deadbabe@gmail.com"
    url = "https://gitlab.com/eliasku/ecxx"
    description = "ecxx: entity component system for c++"
    topics = ("ecs", "entity", "component", "system")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports = ["LICENSE", "README.md"]
    exports_sources = [
        "_scripts/init.cmake",
        "CMakeLists.txt",
        "src*",
        "test*"
    ]
    no_copy_source = True
    scm = {
        "type": "git",
        "url": "https://gitlab.com/eliasku/ecxx.git",
        "revision": "auto"
    }

    def package(self):
        self.copy("*.h", dst="include", src="src/")
        self.copy("*.hpp", dst="include", src="src/")

    def package_id(self):
        self.info.header_only()
